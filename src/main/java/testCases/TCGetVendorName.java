package testCases;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import api.SeleniumBase;
import pages.LoginPage;

public class TCGetVendorName extends SeleniumBase {
	
	@BeforeTest
	public void setData() {
		testCaseName = "TCGetVendorName";
		testDescription = "Vendor Name";
		testNodes = "Name";
		author = "Aravind";
		category= "Smoke";
		
}

	
	@Test
	public void searchVendorName() {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.get("https://acme-test.uipath.com");
		
		new LoginPage()
		.enterLoginName()
		.enterLoginPassword()
		.clickLoginButton()
		.clickSearch()
		.enterID()
		.clickVSearch()
		.getVendorName();
		
	}
	

}
