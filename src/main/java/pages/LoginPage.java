package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import api.SeleniumBase;

public class LoginPage extends SeleniumBase {
	
	public LoginPage() {
		PageFactory.initElements(driver, this);
		
	}
	
	
	@FindBy(how=How.ID,using="email") WebElement email;
	@FindBy(how=How.ID,using="password") WebElement password;
	@FindBy(how=How.ID,using="buttonLogin") WebElement login;
	public LoginPage enterLoginName() {
		clearAndType(email, "rohitharavind29@gmail.com");
		//email.sendKeys("rohitharavind29@gmail.com");
		return this;
	}
	
	public LoginPage enterLoginPassword() {
		clearAndType(password, "lambergoune");
		//password.sendKeys("lambergoune");
		return this;
	}
	
	public DashboardPage clickLoginButton() {
		click(login);
		//login.click();
		return new DashboardPage();
	}

}
