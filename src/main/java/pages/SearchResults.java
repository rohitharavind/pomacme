package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import api.SeleniumBase;

public class SearchResults extends SeleniumBase {
	
	public SearchResults() {
		PageFactory.initElements(driver, this);
		
	}
	
	
	@FindBy(how=How.XPATH,using="//td[text()='Moretronic Intelligence']") WebElement vendorName;
	
	
	public SearchResults getVendorName() {
		String vName = vendorName.getText();
		System.out.println(vName);
		logStep("Vendor Name", "pass");
		return this;
	}
	
	
		
}
