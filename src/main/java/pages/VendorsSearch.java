package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import api.SeleniumBase;

public class VendorsSearch extends SeleniumBase {
	
	public VendorsSearch() {
		PageFactory.initElements(driver, this);
		
	}
	
	
	@FindBy(how=How.ID,using="vendorTaxID") WebElement vendorTaxID;
	@FindBy(how=How.ID,using="buttonSearch") WebElement search;
	
	public VendorsSearch enterID() {
		clearAndType(vendorTaxID, "DE987564");
		//vendorTaxID.sendKeys("DE987564");
		return this;
	}
	
	public SearchResults clickVSearch() {
		click(search);
		//search.click();
		return new SearchResults();
	}	
	
	
		
}
