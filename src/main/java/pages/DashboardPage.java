package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import api.SeleniumBase;

public class DashboardPage extends SeleniumBase {
	
	public DashboardPage() {
		PageFactory.initElements(driver, this);
		
	}
	
	
	@FindBy(how=How.XPATH,using="//button[text()=' Vendors']") WebElement vendor;
	@FindBy(how=How.XPATH,using="//a[text()='Search for Vendor']") WebElement search;
	
	public VendorsSearch clickSearch() {
		Actions builder = new Actions(driver);
		builder.moveToElement(vendor).moveToElement(search).click().build().perform();
		logStep("Click Search", "pass");
		return new VendorsSearch();
	}
	
	
		
}
